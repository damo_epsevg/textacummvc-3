package edu.upc.damo;

import java.util.Iterator;

/**
 * Created by Josep M on 09/10/2014.
 */
public class ModelObservable implements Iterable<CharSequence> {
    // Comportament d'observable

    private OnCanviModelListener observador;
    private Model model;

    public ModelObservable() {
        model = new Model();
    }

    public void setOnCanviModelListener(OnCanviModelListener observador) {
        this.observador = observador;
    }

    private void avisaObservador() {
        if (observador != null)
            observador.onNovesDades();
    }

    public void afegir(CharSequence s) {
        model.afegir(s);
        avisaObservador();
    }

    // Delegació en l'element embolcallat

    /**
     * @param pos Posicio dins del model; base zero
     */
    public void remove(int pos) {
        model.remove(pos);
        avisaObservador();
    }

    public void buida(){
        model.buida();
        avisaObservador();
    }

    @Override
    public Iterator<CharSequence> iterator() {
        return model.iterator();
    }

    public interface OnCanviModelListener {
        void onNovesDades();
    }
}
