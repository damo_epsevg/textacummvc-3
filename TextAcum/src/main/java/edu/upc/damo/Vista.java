package edu.upc.damo;

import android.app.Activity;
import android.widget.TextView;

/**
 * Created by Josep M on 09/10/2014.
 * <p/>
 * Responsable de la presentació
 * Observa el model i s'actualitza quanaquest l'avisa
 */
public class Vista {
    private ModelObservable model;
    private TextView res;

    public Vista(Activity a) {
        res =  a.findViewById(R.id.resultat);

    }

    public void refesPresentacio() {
        res.setText("");
        int i = 0;
        for (CharSequence s : model) {
            if (i != 0)
                res.append("\n");
            res.append(String.valueOf(i + 1));
            res.append(" ");
            res.append(s);
            i++;
        }


    }

    public void defineixModel(ModelObservable model) {
        this.model = model;
        model.setOnCanviModelListener(new ModelObservable.OnCanviModelListener() {
                                          @Override
                                          public void onNovesDades() {
                                              refesPresentacio();
                                          }
                                      }
        );
    }
}
